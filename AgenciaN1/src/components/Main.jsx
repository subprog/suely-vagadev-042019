import React from 'react';

import Footer from './Footer';
import Breadcrumb from './Breadcrumb';
import ProductInfo from './ProductInfo';
import Recommended from './Recommended';



export default class BodyInfo extends React.Component{
    render(){
        return ( 
            <div className='product-info' id='0'>
                <div className='container'>
                    <Breadcrumb />
                    <ProductInfo addIn={this.props.addIn.bind(this)} productData={this.props.productsData[0]} />
                    <Recommended addIn={this.props.addIn.bind(this)} productsData={this.props.productsData}/>
                </div>
                <Footer />
            </div>
        );
    }    
}
