import React from 'react';

export default class ProductTitle extends React.Component {
    render(){
        return(
            <div className='product-title'>
                <h3 className='product-title-text mobile'>{this.props.name}</h3>
            </div>
        );
    }
}