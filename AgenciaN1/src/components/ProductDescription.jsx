import React from 'react'

export default class ProductImg extends React.Component {
    render(){
        return(
            <div className='product-description'>
                <div className='description-title'>
                    <h3>Descrição do produto</h3>
                </div>

                <div className='description-text'>{this.props.description}</div>
            </div>
        );
    }
}